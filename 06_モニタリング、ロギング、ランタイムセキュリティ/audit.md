> ①Policyファイルの作成
~~~
cat > /etc/kubernetes/audit-policy.yaml << EOF 
apiVersion: audit.k8s.io/v1
kind: Policy
omitStages:
 - "RequestReceived"

rules:
- level: Request
  resources:
  - group: ""
    resources: ["pods"]
  namespaces: ["kube-system"]

- level: Metadata
  resources:
  - group: ""
    resources: ["configmap","secret"]

- level: RequestResponse
  resources:
  - group: ""
    resources: ["pods"]

- level: Metadata
    omitStages:
    - "RequestReceived"
EOF
~~~
<br>

> ②監査のバックエンド設定
~~~
vim /etc/kubernetes/manifests/kube-apiserver.yaml
~~~
~~~
    - --audit-log-path=/var/log/audit.log
    - --audit-log-maxage=5
    - --audit-log-maxbackup=10
    - --audit-policy-file=/etc/kubernetes/audit-policy.yaml
~~~
~~~
  - mountPath: /var/log/audit.log
    name: audit-log
    readOnly: false
  - mountPath: /etc/kubernetes/audit-policy.yaml
    name: audit-policy
    readOnly: true
~~~
~~~
  - name: audit-log
    hostPath:
      path: /var/log/audit.log
      type: FileOrCreate
  - name: audit-policy
    hostPath:
      path: /etc/kubernetes/audit-policy.yaml
      type: File
~~~
<br>

> ④排出されたログの確認
~~~
systemctl daemon-reload && systemctl restart kubelet
~~~
~~~
cat /var/log/audit.log
~~~
<br>
