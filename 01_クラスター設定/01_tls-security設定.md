> Ingressに使用する証明書の作成
~~~
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -out tls.crt -keyout tls.key -subj "/CN=ingress.tls.jp"
~~~
<br>

> 疎通確認のPod（Deployment ）とServiceの作成
~~~
kubectl create deployment tls-pod --image=httpd && kubectl expose deployment tls-pod --name=tls-service --port=8080 --target-port=80
~~~
<br>

> Nginx-Ingress-Controllerのインストール
~~~
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.0.4/deploy/static/provider/baremetal/deploy.yaml
~~~
<br>

> Secretの作成
~~~
kubectl create secret tls tls-secret --key tls.key --cert tls.crt
~~~
<br>

> IngressのYamlファイル作成
~~~
kubectl create ingress tls-ingress --class=nginx --rule="ingress.tls.jp/*=tls-service:8080,tls=tls-secret" --dry-run=client -o yaml > ingress.yaml
~~~
<br>

> ファイル編集とIngressリソースのApply
~~~
vim ingress.yaml
kubectl apply -f  ingress.yaml
~~~
<br>

> yamlへ転記を行う際
~~~
cat > ingress.yaml << EOF
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  creationTimestamp: null
  name: tls-ingress
spec:
  ingressClassName: nginx
  rules:
  - host: ingress.tls.jp
    http:
      paths:
      - backend:
          service:
            name: tls-service
            port:
              number: 8080
        path: /
        pathType: Prefix
  tls:
  - hosts:
    - ingress.tls.jp
    secretName: tls-secret
EOF
~~~
<br>

> IPアドレスを設定した「ingress.tls.jp」と紐づけで/etc/hostsへ記載を行う
~~~
vim /etc/hosts
~~~

~~~
127.0.0.1 example.com

# The following lines are desirable for IPv6 capable hosts
::1 ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
ff02::3 ip6-allhosts

173.31.46.14 node01
173.31.46.16 node02 
# 上に記載されているIPアドレスはsshでアクセスを行うために必要なものなので、操作をしないでください。 

173.31.46.167 ingress.tls.jp　# IPアドレスはハンズオン環境ごとに違いがあります。
~~~
<br>

> ハンズオンで使用したDeploymentやService、Ingressなどの削除
~~~
# Ingressの削除
kubectl delete ingress ingress tls-ingress

# DeploymentとServiceの削除 
kubectl delete deployments tls-pod 
kubectl delete svc tls-service

# Secretの削除
kubectl delete secret tls-secret

# Nginx-Ingress-Controllerの削除
kubectl delete -f  https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.0.4/deploy/static/provider/baremetal/deploy.yaml 


# 秘密鍵と証明書の削除 
rm tls.crt 
rm tls.key
~~~
<br>
