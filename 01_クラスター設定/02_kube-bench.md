> kube-benchの適用
~~~
kubectl apply -f https://gitlab.com/t-tashibu/yaml/-/raw/main/kube-bench.yaml
~~~
<br>

> Jobによって作成されたkube-benchのPodから排出されるログの確認
~~~
kubectl logs kube-bench-master-<DEPOYMENT-ID>
~~~
<br>

> コンテナで作成する方法
~~~
docker run --pid=host -v /etc:/etc:ro -v /var:/var:ro -t docker.io/aquasec/kube-bench:latest --version 1.18
~~~
<br>

> kube-benchの削除
~~~
kubectl delete job.batch kube-bench-master
~~~
<br>
