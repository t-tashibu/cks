> ①②AppArmorのプロファイル作成と適用および適用確認
~~~
ssh node01
~~~
~~~
cat > cks-profile << EOF
#include <tunables/global>
profile  cks-profile flags=(attach_disconnected) {
  file,
  # Deny all file writes.
  deny /** w,
}
EOF
~~~
~~~
apparmor_parser -q cks-profile

aa-status | grep cks-profile

exit
~~~
~~~
ssh node02
~~~
~~~
cat > cks-profile << EOF
#include <tunables/global>
profile  cks-profile flags=(attach_disconnected) {
  file,
  # Deny all file writes.
  deny /** w,
}
EOF
~~~
~~~
apparmor_parser -q cks-profile

aa-status | grep cks-profile

exit
~~~
<br>

> ③Yamlファイルの追記とPodのスケジューリング
~~~
cat > cks-apparmor.yaml << EOF
apiVersion: v1
kind: Pod
metadata:
  name: cks-apparmor
spec:
  containers:
  - name: cks-apparmor
    image: busybox
    command: [ "sh", "-c", "echo 'Hello AppArmor!' && sleep 1h" ]
EOF
~~~
~~~
  annotations:
     container.apparmor.security.beta.kubernetes.io/cks-apparmor: localhost/cks-profile
~~~
~~~
kubectl apply -f cks-apparmor.yaml
~~~
<br>

> ④AppArmor適用確認とPodとプロファイル削除
~~~
kubectl exec cks-apparmor -- touch /tmp/test
~~~
~~~
kubectl delete –f cks-apparmor.yaml
kubectl delete pod cks-apparmor
~~~
~~~
ssh node01
apparmor_parser –R cks-profile
aa-status | grep cks-profile
exit
~~~
~~~
ssh node02
apparmor_parser –R cks-profile
aa-status | grep cks-profile
exit
~~~
<br>
