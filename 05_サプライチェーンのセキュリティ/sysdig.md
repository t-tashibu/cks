> ①sysdigのインストール
~~~
ssh node01
~~~
~~~
apt update -y
~~~
~~~
apt install curl gnupg software-properties-common -y
~~~
~~~
curl -s https://s3.amazonaws.com/download.draios.com/stable/install-sysdig | sudo bash
~~~
~~~
sysdig --version
~~~
<br>

> ②-1csysdigコマンド
~~~
csysdig
~~~
<br>

> ②-2 引数を使用したコマンド入力
~~~
sysdig
~~~
~~~
sysdig -p %evt.num,%user.name,%proc.name,%k8s.pod.name
~~~
<br>

> ⑤-1フィルタリングリストの取得
~~~
sysdig -l | grep num
~~~
~~~
sysdig -l | grep user
~~~
~~~
sysdig -l | grep container
~~~
<br>

> ⑤-2 ログの出力
~~~
sysdig -M 1 -p “%evt.num,%user.name”container.name=nginx  
~~~
<br>

> ⑥仮想問題の解答
~~~
sysdig -l | grep time 
~~~
~~~
sysdig -l | grep uid
~~~
~~~
sysdig -l | grep proc
~~~
~~~
sysdig -M 30 -p “%evt.time,%user.uid,%proc.name”container.name=redis  > sysdig.txt
~~~
<br>
