> ①RuntimeClassの有効化
~~~
cp /etc/kubernetes/manifest/kube-apiserver.yaml cp /etc/kubernetes/
vim /etc/kubernetes/manifest/kube-apiserver.yaml

systemctl daemon-reload && systemctl restart kubelet
~~~
<br>

> ②gVisorのインストール
~~~
ssh node01
~~~
~~~
wget -o- https://raw.githubusercontent.com/killer-sh/cks-course-environment/master/course-content/microservice-vulnerabilities/container-runtimes/gvisor/install_gvisor.sh
~~~
~~~
sh install_gvisor.sh
~~~
~~~
exit
~~~
~~~
ssh node02
~~~
~~~
wget -o- https://raw.githubusercontent.com/killer-sh/cks-course-environment/master/course-content/microservice-vulnerabilities/container-runtimes/gvisor/install_gvisor.sh
~~~
~~~
sh install_gvisor.sh
~~~
~~~
exit
~~~
<br>


> ③OCIランタイムの確認
~~~
cat /etc/containerd/config.toml
~~~
<br>


> ④RuntimeClassの作成
~~~
cat > runtimeclass.yaml << EOF
apiVersion: node.k8s.io/v1
kind: RuntimeClass
metadata:
  name: gvisor
handler: runsc
EOF
~~~
~~~
kubectl apply -f runtimeclass.yaml
~~~
~~~
kubectl get runtimeclasses
~~~
<br>


> ⑤-1フォーマットとなるPodのYamlファイル作成
~~~
cat > gvisor.yaml << EOF
apiVersion: v1
kind: Pod
metadata:
  labels:
    run: gvisor
  name: gvisor
spec:
  runtimeClassName: gvisor
  containers:
    - image: nginx
      name: gvisor
      resources: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Always
EOF
~~~
<br>

> ⑤-2 2RuntimeClassを追記したYamlファイルの作成
~~~
vim gvisor.yaml
~~~
~~~
  runtimeClassName: gvisor
~~~
~~~  
kubectl apply -f gvisor.yaml 
~~~
<br>


> ⑥gVisorの機能確認
~~~
kubectl run --image=nginx nginx 
~~~
~~~
kubectl exec -it nginx -- dmesg
~~~
~~~
kubectl exec -it gvisor -- dmesg 
~~~
<br>

> ⑦作成したコンテナとRuntimeClassの削除
~~~
kubectl delete -f gvisor.yaml 
kubectl delete -f　runtimeclass.yaml
kubectl delete pod nginx
~~~
<br>
