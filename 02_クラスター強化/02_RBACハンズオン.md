> ①ClusterRoleの作成
~~~
kubectl create ns cks-ns 
kubectl create clusterrole cks-cs-role --verb=create,list,delete --resource=pod,deploy,pvc --dry-run=client -o yaml > cks-cs-role.yaml
~~~
<br>

> ②Role-bidingの作成
~~~
kubectl create rolebinding cks-r-binding --clusterrole=cks-cs-role --user=k8s-user -n cks-ns
~~~
<br>


> ③権限付与の確認
~~~
kubectl auth can-i create pod --as=k8s-user -n cks-ns

kubectl config use-context k8s-user

kubectl run --image=nginx cks-test --dry-run=client
~~~
<br>

> ④ClusterRoleの権限付与の変更
<br>

> ⑤ServiceAccountの作成と権限付与
~~~
kubectl create sa cks-sa -n cks-ns && kubectl create rolebinding sa-r-rolebinding --clusterrole=cks-cs-role  --serviceaccount=cks-ns:cks-sa -n cks-ns   
~~~
<br>

> ⑥権限付与の確認
~~~
kubectl auth can-i create pod --as=system:serviceaccount:cks-ns:cks-sa -n cks-ns
~~~
<br>
