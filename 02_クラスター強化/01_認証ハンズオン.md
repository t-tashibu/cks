> ①x509証明書の作成
~~~
openssl genrsa -out k8s-user.key 2048
~~~
<br>

> ②公開鍵の作成
~~~
openssl req -new -key k8s-user.key -out k8s-user.csr -subj "/CN=k8s-user"
~~~
<br>

>③公開鍵をBase64へ変換
~~~
cat k8s-user.csr | base64 | tr -d "\n" 
~~~
<br>

> ④公開鍵のYamlファイル作成
~~~
wget -o- https://gitlab.com/t-tashibu/yaml/-/raw/main/cks-user.yaml

vim cks-user.yaml
~~~
<br>

> ⑤Yamlファイルへの追記
<br>

> ⑥公開鍵をApplyして認証局へ登録を行い、その公開鍵の許可を行う。
~~~
kubectl apply –f cks-user.yaml

kubectl certificate approve k8s-user
~~~
<br>

> ⑦kubernetesクラスターにはTLS認証のためにPKI証明書が必要なため、認証局に登録した公開鍵のコードからPKI証明書の作成を行う。
~~~
kubectl get csr k8s-user -o jsonpath='{.status.certificate}'| base64 -d > k8s-user.crt
~~~
<br>

> ⑧kubeconfigファイルに作成した証明書、登録したUser、PKI証明書の登録を行う
~~~
kubectl config set-credentials k8s-user --client-key=k8s-user.key --client-certificate=k8s-user.crt --embed-certs=true
~~~
<br>

> ⑨KubernetesのコンテキストファイルにUserと使用するkubernetesクラスター名の登録と作成したuserの確認
~~~
kubectl config set-context k8s-user --cluster=kubernetes --user=k8s-user

kubectl config get-contexts

~~~
<br>
