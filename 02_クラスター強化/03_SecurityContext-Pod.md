> ①-1Podの作成
~~~
cat > sc-pod.yaml << EOF
apiVersion: v1
kind: Pod
metadata:
  name: security-context-demo
spec:
  securityContext:
    runAsUser: 1000
    fsGroup: 2000
  volumes:
  - name: sec-ctx-vol
    emptyDir: {}
  containers:
  - name: sec-ctx-demo
    image: gcr.io/google-samples/node-hello:1.0
    volumeMounts:
    - name: sec-ctx-vol
      mountPath: /data/demo
EOF
~~~
<br>

> ①-2Yamlファイルの実行
~~~
kubectl apply -f sc-pod.yaml
~~~
<br>

> ②Shellの実行とプロセス確認
~~~
kubectl exec security-context-demo -it -- sh 

ps aux
~~~
<br>

> ③fsGroupの確認 ※Shellの実行はそのままでコンテナの中にいる状態
~~~
cd /data && ls -l

cd /data/demo && echo hello > fsgroupfile

ls –l

exit
~~~
<br>

> ④RunAsUserを設定しないケース
~~~
kubectl run sc-no-user --image=gcr.io/google-samples/node-hello:1.0

kubectl exec sc-no-user -it -- sh  

ps aux

exit
~~~
<br>

