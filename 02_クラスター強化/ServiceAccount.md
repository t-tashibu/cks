> ⓪-1ServiceAccountTokenの確認
~~~
kubectl run --image=nginx nginx 
kubectl get pod nginx -o yaml

kubectl exec -it nginx -- cat /var/run/secrets/kubernetes.io/serviceaccount
~~~
<br>

> ⓪-2ServiceAccountのヘッダーとペイロードの確認
~~~
snap install jwt-decode

jwt-decode.header <serviceaccount-Token>

jwt-decode.payload <serviceaccount-Token>
~~~
<br>

> ①ServiceAccountとNameSpaceの作成
~~~
kubectl create sa cks-sa -n cks-ns
~~~
<br>

> ②-2PodとRoleの作成
~~~
cat > process.yaml << EOF
apiVersion: v1
kind: Pod
metadata:
  name: process-pod
  namespace: cks-ns
spec:
  containers:
  - image: bitnami/kubectl
    name: pod-list
    command:
    - sh
    - -c
    - |
      while true
      do
        kubectl get pod
        sleep 30
      done
  serviceAccountName: cks-sa

---
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  creationTimestamp: null
  name: process-role
  namespace: cks-ns
rules:
- apiGroups:
  - ""
  resources:
  - pods
  verbs:
  - create
  - list
  - get
  - delete
  EOF
~~~
<br>

> ②-3プロセスの権限確認
~~~
kubectl logs -n cks-ns process-pod 
~~~
<br>

> ③Role-Biddingの作成
~~~
kubectl create rolebinding  sa-r-binding --role=process-role --serviceaccount=cks-ns:cks-sa --namespace=cks-ns 
~~~
<br>

> ④権限確認
~~~
kubectl auth can-i get pod --as=system:serviceaccount:cks-ns:cks-sa -n cks-ns

kubectl logs -n cks-ns process-pod 
~~~
<br>

> ⑤Podの削除
~~~

~~~
<br>

> ⑤ハンズオンで使用したものの削除
~~~
kubectl delete -f process.yaml

kubectl delete rolebinding -n cks-ns  sa-r-binding

Kubectl delete ns cks-ns
~~~
<br>

