> ①-1Podの作成とShellの実行
~~~
cat > sc-pod-2.yaml << EOF
apiVersion: v1
kind: Pod
metadata:
  name: security-context-demo-2
spec:
  securityContext:
    runAsUser: 1000
  containers:
  - name: sec-ctx-demo-2
    image: gcr.io/google-samples/node-hello:1.0
    securityContext: 
      runAsUser: 2000
      allowPrivilegeEscalation: false
EOF
~~~
<br>

> ①-2 Podのスケジューリング
~~~
kubectl apply -f sc-pod-2.yaml
~~~
<br>

> ②Shellの実行とプロセス確認
~~~
kubectl exec security-context-demo-2 -it -- sh 

ps aux

exit
~~~
<br>

> ③作成したPodの削除
~~~
kubectl delete -f sc-pod.yaml -f sc-pod-2.yaml
~~~
<br>
